document.addEventListener('DOMContentLoaded', function() {
  const primaryList = $('.header__navigation-list-item.hasChildren'),
    tertiaryList = '.header__navigation-tertiary-list',
    navExpansionIcon = $('.header__navigation-item-wrapper .fa'),
    secondaryList = $('.header__navigation-secondary-list'),
    toggleNavButton = $('.header__menu-button'),
    navWrapper = $('.header__nav-bg'),
    searchBoxButton = $('.search-box-button'),
    searchBoxInput = $('.search-box-input'),
    searchFilter = $('.header__search-filter'),
    searchFilterDropdown = $('.header__search-filter-dropdown');

  function headerFunctionality() {
    let isDesktop = $(window).width() > 991;
    let isTablet = $(window).width() < 992;
    let isMobile = $(window).width() < 768;

    if (isDesktop) {
      desktopFunctionality();
    } else if (isTablet) {
      tabletFunctionality();

      if (isMobile) {
        mobileFunctionality();
      }
    }

    commonFunctionality();
  }

  function commonFunctionality() {
    const isHidden = searchFilterDropdown.is(':hidden');
    if (!isHidden) {
      searchFilter.click();
    }

    searchFilter.click(function() {
      searchFilterDropdown.toggleClass('d-block');

      if (isHidden) {
        searchFilter
          .find('.fa')
          .removeClass('fa-chevron-up')
          .addClass('fa-chevron-down');
      } else {
        searchFilter
          .find('.fa')
          .removeClass('fa-chevron-down')
          .addClass('fa-chevron-up');
      }
    });
  }

  headerFunctionality();

  $(window).resize(function() {
    headerFunctionality();
  });

  function desktopFunctionality() {
    navExpansionIcon.unbind('click');
    searchBoxButton.unbind('click');
    secondaryList.removeAttr('style');
    navWrapper.removeAttr('style');
    navExpansionIcon.removeClass('fa-minus').addClass('fa-plus');
    desktopTertiaryNav();
  }

  function desktopTertiaryNav() {
    primaryList.hover(
      function() {
        $(this)
          .find(tertiaryList)
          .show();
      },
      function() {
        $(this)
          .find(tertiaryList)
          .hide();
      }
    );
  }

  function mobileFunctionality() {
    searchBoxButton.removeAttr('style');
    toggleNavButton.removeAttr('style');
    searchFilter.removeAttr('style');
    searchBoxInput.removeAttr('style');
    searchBoxInput.addClass('d-none');

    searchBoxButton.click(function() {
      const headerWidth = searchBoxInput
        .parent()
        .parent()
        .parent()
        .width();
      searchBoxButton.css({ height: '40px', width: '40px' });
      toggleNavButton.hide();
      searchFilter.css({ display: 'flex', 'z-index': 4, left: '17px' });
      searchBoxInput.removeClass('d-none');
      searchBoxInput.css({
        position: 'absolute',
        left: '15px',
        height: '44px',
      });
      searchBoxInput.animate({ width: headerWidth });
      searchBoxInput.show();
    });
  }

  function tabletFunctionality() {
    searchBoxInput.removeAttr('style');
    searchFilter.removeAttr('style');
    primaryList.unbind('mouseenter mouseleave');
    navExpansionIcon.unbind('click');
    toggleNavButton.removeAttr('style');
    toggleNavButton.unbind('click');
    searchBoxButton.unbind('click');
    tabletTertiaryNav();
  }

  function tabletTertiaryNav() {
    navExpansionIcon.click(function() {
      const dropdown = $(this)
        .parent()
        .next();

      const isHidden = $(dropdown).is(':hidden');

      if (isHidden) {
        $(this)
          .removeClass('fa-plus')
          .addClass('fa-minus');
        dropdown.slideDown();
      } else {
        $(this)
          .removeClass('fa-minus')
          .addClass('fa-plus');
        dropdown.slideUp();
      }
    });

    toggleNavButton.click(function() {
      navWrapper.slideToggle();
    });
  }
});
